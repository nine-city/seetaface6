# Install script for directory: D:/desktop/project/CPP/seetaface6/FaceTracker6/FaceTracking

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "D:/desktop/project/CPP/seetaface6/FaceTracker6/../build")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/cmake" TYPE FILE FILES "D:/desktop/project/CPP/seetaface6/FaceTracker6/cmake/FindSeetaFaceTracking.cmake")
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x64" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/desktop/project/CPP/seetaface6/FaceTracker6/build/lib/x64/Debug/SeetaFaceTracking600.lib")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x64" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/desktop/project/CPP/seetaface6/FaceTracker6/build/lib/x64/Release/SeetaFaceTracking600.lib")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x64" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/desktop/project/CPP/seetaface6/FaceTracker6/build/lib/x64/MinSizeRel/SeetaFaceTracking600.lib")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/x64" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/desktop/project/CPP/seetaface6/FaceTracker6/build/lib/x64/RelWithDebInfo/SeetaFaceTracking600.lib")
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/x64" TYPE SHARED_LIBRARY FILES "D:/desktop/project/CPP/seetaface6/FaceTracker6/build/lib/x64/Debug/SeetaFaceTracking600.dll")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/x64" TYPE SHARED_LIBRARY FILES "D:/desktop/project/CPP/seetaface6/FaceTracker6/build/lib/x64/Release/SeetaFaceTracking600.dll")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/x64" TYPE SHARED_LIBRARY FILES "D:/desktop/project/CPP/seetaface6/FaceTracker6/build/lib/x64/MinSizeRel/SeetaFaceTracking600.dll")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin/x64" TYPE SHARED_LIBRARY FILES "D:/desktop/project/CPP/seetaface6/FaceTracker6/build/lib/x64/RelWithDebInfo/SeetaFaceTracking600.dll")
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/seeta/Common" TYPE FILE FILES
    "D:/desktop/project/CPP/seetaface6/FaceTracker6/FaceTracking/include/seeta/Common/CStruct.h"
    "D:/desktop/project/CPP/seetaface6/FaceTracker6/FaceTracking/include/seeta/Common/Struct.h"
    )
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/seeta" TYPE FILE FILES
    "D:/desktop/project/CPP/seetaface6/FaceTracker6/FaceTracking/include/seeta/CTrackingFaceInfo.h"
    "D:/desktop/project/CPP/seetaface6/FaceTracker6/FaceTracking/include/seeta/FaceTracker.h"
    )
endif()

