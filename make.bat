@echo off

set "modules=OpenRoleZoo SeetaAuthorize TenniS FaceBoxes FaceRecognizer6 FaceTracker6 Landmarker PoseEstimator6 QualityAssessor3 SeetaAgePredictor SeetaEyeStateDetector SeetaGenderPredictor SeetaMaskDetector FaceAntiSpoofingX6"

for %%d in (%modules%) do (
    pushd %%d\craft
    call build.win.vc14.x64.cmd
    popd
)
