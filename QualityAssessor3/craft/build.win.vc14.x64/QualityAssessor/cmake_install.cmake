# Install script for directory: D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64/SeetaQualityAssessor300.lib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/desktop/project/CPP/seetaface6/QualityAssessor3/build/lib/x64/Debug/SeetaQualityAssessor300.lib")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64/SeetaQualityAssessor300.lib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/desktop/project/CPP/seetaface6/QualityAssessor3/build/lib/x64/Release/SeetaQualityAssessor300.lib")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64/SeetaQualityAssessor300.lib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/desktop/project/CPP/seetaface6/QualityAssessor3/build/lib/x64/MinSizeRel/SeetaQualityAssessor300.lib")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64/SeetaQualityAssessor300.lib")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64" TYPE STATIC_LIBRARY OPTIONAL FILES "D:/desktop/project/CPP/seetaface6/QualityAssessor3/build/lib/x64/RelWithDebInfo/SeetaQualityAssessor300.lib")
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64/SeetaQualityAssessor300.dll")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64" TYPE SHARED_LIBRARY FILES "D:/desktop/project/CPP/seetaface6/QualityAssessor3/build/lib/x64/Debug/SeetaQualityAssessor300.dll")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Rr][Ee][Ll][Ee][Aa][Ss][Ee])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64/SeetaQualityAssessor300.dll")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64" TYPE SHARED_LIBRARY FILES "D:/desktop/project/CPP/seetaface6/QualityAssessor3/build/lib/x64/Release/SeetaQualityAssessor300.dll")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Mm][Ii][Nn][Ss][Ii][Zz][Ee][Rr][Ee][Ll])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64/SeetaQualityAssessor300.dll")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64" TYPE SHARED_LIBRARY FILES "D:/desktop/project/CPP/seetaface6/QualityAssessor3/build/lib/x64/MinSizeRel/SeetaQualityAssessor300.dll")
  elseif(CMAKE_INSTALL_CONFIG_NAME MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
     "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64/SeetaQualityAssessor300.dll")
    if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
      message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
    endif()
    file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/lib/x64" TYPE SHARED_LIBRARY FILES "D:/desktop/project/CPP/seetaface6/QualityAssessor3/build/lib/x64/RelWithDebInfo/SeetaQualityAssessor300.dll")
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/Common/CStruct.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/Common/Struct.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/Common" TYPE FILE FILES
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/Common/CStruct.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/Common/Struct.h"
    )
endif()

if(CMAKE_INSTALL_COMPONENT STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/CStruct.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/Struct.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/QualityAssessor.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/QualityOfBrightness.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/QualityOfClarity.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/QualityOfIntegrity.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/QualityOfLBN.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/QualityOfPose.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/QualityOfPoseEx.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/QualityOfResolution.h;D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta/QualityStructure.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  file(INSTALL DESTINATION "D:/desktop/project/CPP/seetaface6/QualityAssessor3/../build/include/seeta" TYPE FILE FILES
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/Common/CStruct.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/Common/Struct.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/QualityAssessor.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/QualityOfBrightness.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/QualityOfClarity.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/QualityOfIntegrity.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/QualityOfLBN.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/QualityOfPose.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/QualityOfPoseEx.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/QualityOfResolution.h"
    "D:/desktop/project/CPP/seetaface6/QualityAssessor3/QualityAssessor/include/seeta/QualityStructure.h"
    )
endif()

